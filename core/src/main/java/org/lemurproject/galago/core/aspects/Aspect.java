package org.lemurproject.galago.core.aspects;

import java.util.Map;
import java.util.Set;

public class Aspect implements Comparable<Aspect> {

    //Average score of all named entities of aspect
    private Double average_score;
    private String aspect_name;
    //Number of entities in this aspect
    private Integer num_entities;
    //Aspect priority is used to determine the rank of aspect.
    //Higher the priority ...lower the rank...
    private Double aspect_priority;

    //Cosine distance between aspect name and query term
    private Double cosine_distance_with_query_term;

    // Map of <entity ,score>
    // Here score is the priority of that entity....
    // score is caluculated based on cosine distance of this named entity with query term  and highest document relavance and idf
    private Map<String,Double> map_of_entities;


    public Double getCosine_distance_with_query_term() {
        return cosine_distance_with_query_term;
    }

    public void setCosine_distance_with_query_term(Double cosine_distance_with_query_term) {
        this.cosine_distance_with_query_term = cosine_distance_with_query_term;
    }

    public Map<String, Double> getMap_of_entities() {
        return map_of_entities;
    }


    public Aspect(String aspect_name, Map<String,Double> map_of_entities)
    {
        this.aspect_name = aspect_name;
        this.map_of_entities=map_of_entities;
    }

    public void caluculateAverage()
    {
        double total_score = 0;
        for(Map.Entry<String,Double> entry:this.map_of_entities.entrySet())
        {
            total_score += entry.getValue();
        }
        this.average_score = total_score/this.map_of_entities.size();
    }



    public void caluculateNumberOfEntities()
    {
        this.num_entities = this.map_of_entities.size();
    }

    //Caluculation of priority of this aspect
    //Change this function to experiment with different priority heuristics
    public void caluculateAspectPriority()
    {
        this.aspect_priority = (this.num_entities * this.average_score) / this.cosine_distance_with_query_term;
    }

    public Double getAverage_score() {
        return average_score;
    }

    public void setAverage_score(Double average_score) {
        this.average_score = average_score;
    }

    public String getAspect_name() {
        return aspect_name;
    }

    public void setAspect_name(String aspect_name) {
        this.aspect_name = aspect_name;
    }

    public Integer getNum_entities() {
        return num_entities;
    }

    public void setNum_entities(Integer num_entities) {
        this.num_entities = num_entities;
    }

    public Double getAspect_priority() {
        return aspect_priority;
    }

    public void setAspect_priority(Double aspect_priority) {
        this.aspect_priority = aspect_priority;
    }

    public void setMap_of_entities(Map<String, Double> map_of_entities) {
        this.map_of_entities = map_of_entities;
    }

    public String getAspectName() {
        return aspect_name;
    }

    @Override
    public boolean equals(Object obj) {
        Aspect a = (Aspect)obj;
        return this.aspect_name.equals(a.getAspectName());
    }


    // Change this function to change order of aspect
    @Override
    public int compareTo(Aspect o) {
        return this.cosine_distance_with_query_term.compareTo(o.cosine_distance_with_query_term);
    }

    // Aspectt is made into a string
    // This function is used to log aspect details in log file
    @Override
    public String toString()
    {
        String ans="";
        ans = ans+"Aspect Name  : "+this.aspect_name+"\n";
        ans = ans+"Average score of Aspect : "+this.average_score+"\n";
        ans = ans+"Number of entities in this aspect : "+this.num_entities+"\n";
        ans = ans+"Cosine distance with query term is : "+this.cosine_distance_with_query_term+"\n";
        ans = ans+"Priority of this aspect is : "+this.aspect_priority+"\n";
        ans = ans+"The Named entities in this aspect and their scores are : \n";

        for(Map.Entry<String,Double> entry: this.map_of_entities.entrySet())
        {
            ans= ans+entry.getKey()+" : "+entry.getValue()+"\n";
        }
        return ans;
    }
}
