package org.lemurproject.galago.core.aspects;

import static org.lemurproject.galago.core.clustering.UtilityFunctions.cosine;
import static org.lemurproject.galago.core.clustering.UtilityFunctions.euclideanDistance;
import org.lemurproject.galago.core.tools.Search.SearchResult;
import org.lemurproject.galago.core.tools.Search.SearchResultItem;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.StringTokenizer;
import java.util.*;
import java.util.Map.*;
import javatools.parsers.PlingStemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer;

public class FindAspects {

    // Generally galago returns document scores as negative values ,but highest document score corresponds to most relavent document
    // In order to make scores uniform and usable for priority caluculation , all the top 100 document scores are normalized to values between 0 and 1
    public static void normalize_document_scores(SearchResult result)
    {
        double min_score = Double.MAX_VALUE;
        double max_score = Double.MIN_VALUE;

        for(SearchResultItem item : result.items)
        {
            if(item.score < min_score)
                min_score = item.score;
            if(item.score > max_score)
                max_score = item.score;
        }

        for(SearchResultItem item : result.items)
        {
            item.score = (item.score-min_score)/(max_score-min_score);
        }
        return;
    }

    // LinkedHashMap is special type of hashmap which preserve order of insertion
    // This function is used to sort a hashmap based on values...
    // Here we need to sort based on values not keys ,hence we need to use this function instead of built in Collections.sort()
    public static LinkedHashMap sortHashMapByValues(Map passedMap,boolean ascending) {
        List mapKeys = new ArrayList<>(passedMap.keySet());
        List mapValues = new ArrayList<>(passedMap.values());

        if(ascending)
            Collections.sort(mapValues);
        else
            Collections.sort(mapValues,Collections.reverseOrder());

        LinkedHashMap sortedMap = new LinkedHashMap<>();

        Iterator valueIt = mapValues.iterator();


        while (valueIt.hasNext()) {
            Object val = valueIt.next();
            Iterator keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Object key = keyIt.next();
                Object comp1 = passedMap.get(key);
                Object comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }

    // A function to determine direct aspects
    // It returns Map<aspect name ,list of named entities in this apsect>
    public static Map<String,List<String>> determine_direct_relations(SearchResult result) throws Exception
    {
        //The final object to be returned
        // Map<aspect name, list of named entities int hsi aspect>
        Map<String,List<String>> non_typeof_relations = new HashMap<>();
        // Map<entity_name,apsect_name> with all the entities related to query string via some relation(relation name is aspect name)
        Map<String,String> entity_aspect_name_map = new HashMap<>();
        /* Map<entity_name,aspect_name> with only entitties which are tagged in top k documents ....No of times it is tagged and document relavance are not considered
         here because we have very less no of tagged entities*/
        Map<String,String> required_entity_aspect_name_map = new HashMap<>();

        String query_string = result.queryAsString;
     //   System.out.println(" QUERY STRING "+query_string);
        FileReader nontypeof_file_reader = new FileReader("/home/hemanth/iitd/teknowbase_nontypeof_lookup_table_all.tsv");
        BufferedReader nontypeof_buffered_reader = new BufferedReader(nontypeof_file_reader);
    //    System.out.println(" Non Type of Rlations File reader is created ");

        String current_line;
        while((current_line = nontypeof_buffered_reader.readLine())!=null)
        {
            String[] non_typeof_splitted = current_line.split("\\s+");
            if(non_typeof_splitted.length<3)
                continue;
            String key = non_typeof_splitted[0].replace('_',' ');
            String value = non_typeof_splitted[2].replace('_',' ');
            String relation  = non_typeof_splitted[1].replace('_',' ');
            //System.out.println(key+" : "+value);

            if(value.equals(query_string))
            {
             //   System.out.println("Value is equalled");
                String relation_modified = relation+" "+query_string;
                entity_aspect_name_map.put(key,relation_modified);
            }
            else if(key.equals(query_string))
            {
              //  System.out.println("Key is equalled");
                String relation_modified = query_string+" is "+relation;
                entity_aspect_name_map.put(value,relation_modified);
            }
        }

     //   System.out.println(" Trie building is started ");
        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String entity : entity_aspect_name_map.keySet())
        {
            trie_builder = trie_builder.addKeyword(entity);
        }
        Trie trie = trie_builder.build();
      //  System.out.println(" Trie building has been finished ");
        int count=0;
        for(SearchResultItem item : result.items)
        {
            count++;
         //   System.out.println("Document "+count+" is processed ");
            Collection<Emit> named_entity_occurences = trie.parseText(item.document.text);
         //   System.out.println("Tagged Named Entities in  document"+count+" are : ");
            // Here set is created additionally to handle cases where named entity is present more than once in a document i.e it is used to remove duplicates
            Set<String> named_entities = new HashSet<String>();

            for(Emit named_entity_occurence : named_entity_occurences)
            {
                named_entities.add(named_entity_occurence.getKeyword());
            }

            for(String named_entity : named_entities)
            {
                required_entity_aspect_name_map.put(named_entity,entity_aspect_name_map.get(named_entity));
            }

        }

        for(Map.Entry<String,String> entity_type_pair : required_entity_aspect_name_map.entrySet())
        {
            if(non_typeof_relations.containsKey(entity_type_pair.getValue()))
                non_typeof_relations.get(entity_type_pair.getValue()).add(entity_type_pair.getKey());
            else
            {
                List<String> temp_list = new ArrayList<>();
                temp_list.add(entity_type_pair.getKey());
                non_typeof_relations.put(entity_type_pair.getValue(),temp_list);
            }
        }

    //    non_typeof_relations.put("application",new ArrayList<String>());
      //  System.out.println("size of non-typeof relations is: "+non_typeof_relations.size());
        return non_typeof_relations;

    }



    public static Map<String, Aspect> determine_aspects(SearchResult result) throws Exception
    {
      //  System.out.println(" Determine Aspects method has been started");

      //  System.out.println(" Normalizing results : ");
        normalize_document_scores(result);
      //  System.out.println("Normalization has been finished");

      //  System.out.println("Document number and their galago search engine score is presented below ");
        int count = 0;
        for(SearchResultItem item : result.items)
        {
        //    System.out.println(" Document "+count+" score is "+item.score);
            count++;
        }

        //SnowballStemmer st = new SnowballStemmer(SnowballStemmer.ALGORITHM.ENGLISH);


        Double num_documents = new Double(result.items.size());
        String query_string = result.queryAsString;
        Integer query_string_id = null;
        Integer query_string_kb_id = null;
        ArrayList<Double> query_string_embedding =null;
        ArrayList<Double> query_string_kb_embedding = null;

        FileReader categories_file_reader = new FileReader("/home/hemanth/iitd/galago_lookup_table.txt");
        BufferedReader categories_buffered_reader = new BufferedReader(categories_file_reader);
      //  System.out.println(" Categories File reader is created ");

        FileReader teknowbase_file_reader = new FileReader("/home/hemanth/iitd/teknowbase_lookup_table.txt");
        BufferedReader teknowbase_buffered_reader = new BufferedReader(teknowbase_file_reader);
      ////  System.out.println(" Teknowbase File reader is created ");

        FileReader text_entities_file_reader = new FileReader("/home/hemanth/iitd/galago_mappings.txt");
        BufferedReader text_entities_buffered_reader = new BufferedReader(text_entities_file_reader);
     //   System.out.println(" Text Entities File reader is created ");

        FileReader text_entities_embedding_file_reader = new FileReader("/home/hemanth/iitd/galago_embeddings.txt");
        BufferedReader text_entities_embedding_buffered_reader = new BufferedReader(text_entities_embedding_file_reader);
     //   System.out.println(" Text Entities Embedding File reader is created ");

        FileReader aspects_file_reader = new FileReader("/home/hemanth/iitd/teknowbase_mappings.txt");
        BufferedReader aspects_buffered_reader = new BufferedReader(aspects_file_reader);
     //   System.out.println(" Aspects File reader is created ");

        FileReader aspects_embedding_file_reader = new FileReader("/home/hemanth/iitd/teknowbase_embeddings.txt");
        BufferedReader aspects_embedding_buffered_reader = new BufferedReader(aspects_embedding_file_reader);
     //   System.out.println(" Aspects Embedding File reader is created ");
        BufferedWriter bw = new BufferedWriter(new FileWriter("/mnt/dell/Hemanth/iitd/tagged_entities.txt"));
        BufferedReader br2 = new BufferedReader(new FileReader("/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_entity_ids_changed.tsv"));
        String line1;
        HashMap<String, String> h = new HashMap<String, String>();
        while((line1=br2.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line1,"\t");
            ArrayList<String> list1 = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                list1.add(tok.nextToken());
            }
            if(list1.size()<2) continue;
            h.put(list1.get(0).replace("_"," "),list1.get(1).replace("_"," "));
        }

               int c=0;
        Map<String, ArrayList<Double>> application_embedding_map = new HashMap<String, ArrayList<Double>>(); //map of entity and its application_embedding
        Map<String, ArrayList<Double>> technique_embedding_map = new HashMap<String, ArrayList<Double>>();
        Map<String, ArrayList<Double>> implementation_embedding_map = new HashMap<String, ArrayList<Double>>();
        Map<String, ArrayList<Double>> algorithm_embedding_map = new HashMap<>();
        Map<String,String> teknowbase_map = new HashMap<String,String>();// map of entity name and its category ( extracted from teknowbase and list of pages )

        Map<String,Aspect> aspect_map = new HashMap<String,Aspect>();
        Map<String,String> top_k_named_entities = new LinkedHashMap<String,String>(); // map of entity name and its category for top k entites based on their entity scor
        // Here Linked Hash Map is created so as to preserve sorted order of named entities
        Map<String,Double> entity_score_map = new HashMap<String,Double>(); // map of entity name and its score
        Map<String,Double> entity_idf_map = new HashMap<String,Double>(); // map of entity name and its idf
        Map<String,Integer> entity_name_id_map = new HashMap<String,Integer>(); //map of entity name and its id ( if that entity is present in text entities file )
        Map<Integer,String> entity_id_name_map = new HashMap<Integer,String>(); // map of tagged entity id and its name (a required entity is one which is present in textentities and is also tagged from search results)
        Map<String,ArrayList<Double>> entity_name_embedding_map = new HashMap<String,ArrayList<Double>>();// map of tagged entity name and its embedding
        Map<String,Double> entity_name_euclidean_distance_map = new HashMap<String,Double>(); // map of tagged entity name and its euclidean distance with query string
        Map<String,Double> entity_idf_euclidean_distance_product_map = new HashMap<String,Double>();// map of required entity name and product of its idf and euclidean distance with query string
        Map<String,ArrayList<Double>> aspect_name_embedding_map = new HashMap<>();
        Map<Integer,String> aspect_id_name_map = new HashMap<>();
        Map<String,Integer> aspect_name_id_map = new HashMap<>();


        String current_line;
        while((current_line = text_entities_buffered_reader.readLine())!=null)
        {
            String[] text_entity_name_id_pair = current_line.split("\\s+");
            String key = text_entity_name_id_pair[1].replace('_',' ').toString();
            Integer value = Integer.parseInt(text_entity_name_id_pair[0]);
            entity_name_id_map.put(key,value);
        }
     //   System.out.println(" text entity map is created");




        query_string_id = entity_name_id_map.get(query_string);//When Query String is not present in embeddings ,it will return null,so handle that case in future when required
     //   System.out.println(" Query string is : "+query_string);
     //   System.out.println(" Query string Id is : "+query_string_id);


    //    System.out.println(" Trie building is started ");
        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String keyword : entity_name_id_map.keySet())
        {
            trie_builder = trie_builder.addKeyword(keyword);
        }
        Trie trie = trie_builder.build();
     //   System.out.println(" Trie building has been finished ");



        count=0;
        for(SearchResultItem item : result.items)
        {
            count++;

        //    System.out.println("Document "+count+" is processed ");
            Collection<Emit> named_entity_occurences = trie.parseText(item.document.text.replace("_"," "));
         //   System.out.println("Tagged Named Entities in  document"+count+" are : ");
            Set<String> named_entities = new HashSet<String>(); // Here set is created additionally to handle cases where named entity is present more than once in a document

            for(Emit named_entity_occurence : named_entity_occurences)
            {
                named_entities.add(named_entity_occurence.getKeyword());
            }

            for(String named_entity : named_entities)
            {
            //    System.out.print(named_entity+"\n");
                Double named_entity_score = entity_score_map.get(named_entity);
                if(named_entity_score==null)
                {
                    entity_score_map.put(named_entity,item.score);
                    entity_idf_map.put(named_entity,new Double(1));
                }
                else
                {
                    entity_score_map.put(named_entity,(named_entity_score>item.score)?named_entity_score:item.score);
                    entity_idf_map.put(named_entity,1+entity_idf_map.get(named_entity));
                }
                Integer named_entity_id = entity_name_id_map.get(named_entity);
                if(named_entity_id!=null)
                {
                    //System.out.println(named_entity+"\thas an id described in galago mappings\n");
                    bw.write(named_entity+"\thas an id described in galago mappings\t"+named_entity_id+"\n");
                    entity_id_name_map.put(named_entity_id,named_entity);// This map is created because named entity id acts as key for fetching embedding
                }
            }

        }

        for(String e:entity_score_map.keySet())
        {
            bw.write(e+"\n");
          //  System.out.println(e);
        }

     //   System.out.println(" Entity name  score map has been created");
     //   System.out.println(" Entity id  name map has been created");


        while((current_line = text_entities_embedding_buffered_reader.readLine())!=null)
        {

            String[] embedding = current_line.split("\\s+");
            Integer key = Integer.parseInt(embedding[0]);
            ArrayList<Double> value = new ArrayList<Double>();
            for(int i=1;i<embedding.length;i++)
            {
                value.add(Double.valueOf(embedding[i]));
            }
            bw.write("key found is "+key+"\n");
           // System.out.println("Key found is "+key);
            String named_entity = entity_id_name_map.get(key);

            if(named_entity!=null)
            {
                entity_name_embedding_map.put(named_entity, value);
                bw.write(named_entity+"\tentities with embedding\n");
              //  System.out.println(named_entity+"\tentities with embedding\n");
            }

            if(key.equals(query_string_id))
            {
                query_string_embedding = value;
              //  System.out.println(" Query string embedding is "+value.toString());
            }

        }

      //  System.out.println(" entity_name_embedding_map has been created");


        if(query_string_embedding==null)
            System.out.println(" Query string is not obtained and ist value is null");
        //BufferedReader br3 = new BufferedReader(new FileReader("/mnt/dell/prajna/data/dbpedia/allDbpediaFiles/embeddings/technique.emd.txt"));
        BufferedReader br4 = new BufferedReader(new FileReader("/mnt/dell/prajna/data/dbpedia/allDbpediaFiles/embeddings/implementation_mapped.emd.txt"));

        BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/prajna/data/dbpedia/allDbpediaFiles/embeddings/application_mapped.emd.txt"));
        BufferedReader br5 = new BufferedReader(new FileReader("/mnt/dell/prajna/data/dbpedia/allDbpediaFiles/embeddings/algorithm_mapped.emd.txt"));


        //this part reads the application embedding file

        String line;
        while((line=br1.readLine())!=null)
        {
            if(c==0)
            {
                c++;
                continue;
            }
            else
            {
                c++;
                StringTokenizer tok = new StringTokenizer(line," ");
                String cc;
                cc = tok.nextToken().replace("_"," ");
                //	System.out.println(cc);
                String entity = h.get(cc);

                //	System.out.println(cc+"\t"+entity);
                //  LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
                int d = 0;
                ArrayList<Double> vector = new ArrayList<Double>();
                while(tok.hasMoreTokens())
                {
                    Double val = Double.parseDouble(tok.nextToken());
                    vector.add(val);

                }
                //System.out.println(entity+" mixture size: "+lm.mixture.size());
                application_embedding_map.put(entity,vector);
            }
        }

        //this part reads the algorithm embedding file

        c=0;
        while((line=br5.readLine())!=null)
        {
            if(c==0)
            {
                c++;
                continue;
            }
            else
            {
                c++;
                StringTokenizer tok = new StringTokenizer(line," ");
                String cc;
                cc = tok.nextToken().replace("_"," ");
                //	System.out.println(cc);
                String entity = h.get(cc);

                //	System.out.println(cc+"\t"+entity);
                //  LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
                int d = 0;
                ArrayList<Double> vector = new ArrayList<Double>();
                while(tok.hasMoreTokens())
                {
                    Double val = Double.parseDouble(tok.nextToken());
                    vector.add(val);

                }
                //System.out.println(entity+" mixture size: "+lm.mixture.size());
                algorithm_embedding_map.put(entity,vector);
            }
        }

        //this part reads the implementation embedding file

        c=0;
        while((line=br4.readLine())!=null)
        {
            if(c==0)
            {
                c++;
                continue;
            }
            else
            {
                c++;
                StringTokenizer tok = new StringTokenizer(line," ");
                String cc;
                cc = tok.nextToken().replace("_"," ");
                //	System.out.println(cc);
                String entity = h.get(cc);

                //	System.out.println(cc+"\t"+entity);
                //  LanguageModelEntity lm = new LanguageModelEntity(entity, a, new HashMap<String, Double>(), new HashMap<String, HashMap<String, Double>>(), new HashMap<String, Double>());
                int d = 0;
                ArrayList<Double> vector = new ArrayList<Double>();
                while(tok.hasMoreTokens())
                {
                    Double val = Double.parseDouble(tok.nextToken());
                    vector.add(val);

                }
                //System.out.println(entity+" mixture size: "+lm.mixture.size());
                implementation_embedding_map.put(entity,vector);
            }
        }

        //declare the map to store cosine similarity between the entities based on different types of embeddings

        Map<String, Double> cosine_distance_application_map = new HashMap<String, Double>();
        Map<String, Double> cosine_distance_technique_map = new HashMap<String, Double>();
        Map<String, Double> cosine_distance_implementation_map = new HashMap<String, Double>();
        Map<String, Double> cosine_distance_algorithm_map = new HashMap<String, Double>();

        //calculate euclidean distance for application embedding

        for(String named_entity : application_embedding_map.keySet())
        {
            Double distance;
            if(query_string_embedding!=null) {
                distance = euclideanDistance(query_string_embedding, application_embedding_map.get(named_entity));
                if(entity_idf_map.get(named_entity)!=null && entity_score_map.get(named_entity)!=null)
                 cosine_distance_application_map.put(named_entity, distance*entity_idf_map.get(named_entity));
                else
                    cosine_distance_application_map.put(named_entity,999999.0);
            }

        }

        //calculate euclidean distance for technique embedding

        for(String named_entity : technique_embedding_map.keySet())
        {
            Double distance;
            if(query_string_embedding!=null) {
                distance = euclideanDistance(query_string_embedding, technique_embedding_map.get(named_entity));
                if(entity_idf_map.get(named_entity)!=null && entity_score_map.get(named_entity)!=null)
                    cosine_distance_technique_map.put(named_entity, distance*entity_idf_map.get(named_entity));
                else
                    cosine_distance_technique_map.put(named_entity,999999.0);
            }

        }

        //calculate euclidean distance for implementation embedding

        for(String named_entity : implementation_embedding_map.keySet())
        {
            Double distance;
            if(query_string_embedding!=null)
            {

                distance = euclideanDistance(query_string_embedding, implementation_embedding_map.get(named_entity));
                if(entity_idf_map.get(named_entity)!=null && entity_score_map.get(named_entity)!=null)
                    cosine_distance_implementation_map.put(named_entity, distance*entity_idf_map.get(named_entity));
                else
                    cosine_distance_implementation_map.put(named_entity,999999.0);
            }

        }

        //calculate euclidean distance for algorithm embedding

        for(String named_entity : algorithm_embedding_map.keySet())
        {
            Double distance;
            if(query_string_embedding!=null)
            {

                distance = euclideanDistance(query_string_embedding, algorithm_embedding_map.get(named_entity));
                if(entity_idf_map.get(named_entity)!=null && entity_score_map.get(named_entity)!=null)
                    cosine_distance_algorithm_map.put(named_entity, distance*entity_idf_map.get(named_entity));
                else
                    cosine_distance_algorithm_map.put(named_entity,999999.0);
            }

        }



        for(String named_entity : entity_name_embedding_map.keySet())
        {
            Double distance;
            if(query_string_embedding!=null) {
                distance = euclideanDistance(query_string_embedding, entity_name_embedding_map.get(named_entity));
                entity_name_euclidean_distance_map.put(named_entity, distance);
            }

        }
    //    System.out.println(" entity name euclidean distance map has been created");


        /*By this point all basic maps are been created*/

     //   System.out.println(" entity names and number of documents in which it occured is presented below\n\n\n");
     //   LinkedHashMap<String,Double> entity_idf_map_sorted = sortHashMapByValues(entity_idf_map,false);

        /*for(Map.Entry<String,Double> entry : entity_idf_map_sorted.entrySet())
        {
          //  System.out.println("Entity : "+entry.getKey() + " ( "+ entry.getValue() +" ) ");
        }*/


    //    System.out.println(" entity names and their scores is presented below\n\n\n");
    //    LinkedHashMap<String,Double> entity_score_map_sorted = sortHashMapByValues(entity_score_map,false);
       /* for(Map.Entry<String,Double> entry : entity_score_map_sorted.entrySet())
        {
         //   System.out.println("Entity : "+entry.getKey() + "    ("+ entry.getValue() +") ");
        }*/


    //    System.out.println(" entity names and their cosine similarirty is presented below\n\n\n");
     //   LinkedHashMap<String,Double> entity_name_euclidean_distance_map_sorted = sortHashMapByValues(entity_name_euclidean_distance_map,true);
        /*for(Map.Entry<String,Double> entry : entity_name_euclidean_distance_map_sorted.entrySet())
        {
     //       System.out.println("Entity : "+entry.getKey() + "    ("+ entry.getValue() +") ");
        }*/

        Comparator<Map.Entry<String, Double>> valueComparator = new Comparator<Map.Entry<String, Double>>()
        {
            public int compare(Map.Entry<String, Double> e1, Map.Entry<String, Double> e2)
            {
                Double v1 = e1.getValue();
                Double v2 = e2.getValue();
                return v1.compareTo(v2);
            }
        };

        ArrayList<Entry<String, Double>> cosine_distance_application_map_list = new ArrayList<Entry<String, Double>>(cosine_distance_application_map.entrySet());
        ArrayList<Entry<String, Double>> cosine_distance_technique_map_list = new ArrayList<Entry<String, Double>>(cosine_distance_technique_map.entrySet());
        ArrayList<Entry<String, Double>> cosine_distance_implementation_map_list = new ArrayList<Entry<String, Double>>(cosine_distance_implementation_map.entrySet());
        ArrayList<Entry<String, Double>> cosine_distance_algorithm_map_list = new ArrayList<Entry<String, Double>>(cosine_distance_algorithm_map.entrySet());


        Collections.sort(cosine_distance_application_map_list, valueComparator);
        Collections.sort(cosine_distance_technique_map_list, valueComparator);
        Collections.sort(cosine_distance_implementation_map_list, valueComparator);
        Collections.sort(cosine_distance_algorithm_map_list, valueComparator);
        //sort the hashmaps of different types of embeddings

    //    LinkedHashMap<String, Double> cosine_distance_application_map_sorted = sortHashMapByValues(cosine_distance_application_map,true);


     //   LinkedHashMap<String, Double> cosine_distance_technique_map_sorted = sortHashMapByValues(cosine_distance_technique_map,true);


     //   LinkedHashMap<String, Double> cosine_distance_implementation_map_sorted = sortHashMapByValues(cosine_distance_implementation_map,true);

        for(Map.Entry<String,Double> entry : entity_idf_map.entrySet())
        {
            String key = entry.getKey();
            Double value = Math.log(num_documents/entry.getValue());
            entity_idf_map.put(key,value);
            if(query_string_embedding!=null)
            {
                Double distance = entity_name_euclidean_distance_map.get(key);
                //Double doc_weightage = entity_score_map.get(key);
                if(distance!=null && !distance.equals(new Double(0)))
                    entity_idf_euclidean_distance_product_map.put(key,value/distance);
            }
        }
        //System.out.println(" entity_idf_euclidean_distance_product_map has been created ");


      //  System.out.println(" Entities and their idf_eucidean_distance_product_scores are presented below\n\n\n");
        LinkedHashMap<String,Double> entity_idf_euclidean_distance_product_map_sorted = sortHashMapByValues(entity_idf_euclidean_distance_product_map,false);

        for(Map.Entry<String,Double> entry : entity_idf_euclidean_distance_product_map_sorted.entrySet())
        {
            //System.out.println("Entity : "+entry.getKey() + "     ( "+ entry.getValue()+" ) ");
            bw.write("Entity : "+entry.getKey() + "     ( "+ entry.getValue()+" ) ");
        }


        while((current_line = teknowbase_buffered_reader.readLine())!=null)
        {
            String[] category_entity_pair = current_line.split("\\s+");
            category_entity_pair[0] = category_entity_pair[0].replace('_',' ').toString();
            category_entity_pair[1] = category_entity_pair[1].replace('_',' ').toString();
            String type;
            if((type=teknowbase_map.get(category_entity_pair[0]))!=null)
            {
                teknowbase_map.put(category_entity_pair[0],type+"/"+category_entity_pair[1]);
            }
            else {
                teknowbase_map.put(category_entity_pair[0], category_entity_pair[1]);
            }
        }
      //  System.out.println(" teknowbase is read \n ");

        while((current_line = categories_buffered_reader.readLine())!=null)
        {
            String[] category_entity_pair = current_line.split("\\s+");
            category_entity_pair[0] = category_entity_pair[0].replace('_',' ').toString();
            category_entity_pair[1] = category_entity_pair[1].replace('_',' ').toString();
            String type;
            if((type=teknowbase_map.get(category_entity_pair[0]))!=null)
            {
                teknowbase_map.put(category_entity_pair[0],type+"/"+category_entity_pair[1]);

            }
            else
                teknowbase_map.put(category_entity_pair[0],category_entity_pair[1]);
        }
      //  System.out.println("List of entities from wikipedia pages are read \n ");
      //  System.out.println("Teknowbase map creation is completed\n");

      //  System.out.println(" No of entities for which types are extracted from teknowbase is "+teknowbase_map.size());

        for (Map.Entry<String, Double> entry : entity_idf_euclidean_distance_product_map_sorted.entrySet()) {
            String type;
            if((type = teknowbase_map.get(entry.getKey()))!=null){
                String[] aspect_names = type.split("/");
                for(String aspect_name :aspect_names)
                {
                    Aspect curr_aspect;
                    if((curr_aspect =  aspect_map.get(aspect_name))!=null)
                    {
                        curr_aspect.getMap_of_entities().put(entry.getKey(),entry.getValue());
                    }
                    else {
                        Map<String,Double> temp_map = new HashMap<String,Double>();
                        temp_map.put(entry.getKey(),entry.getValue());
                        Aspect new_aspect = new Aspect(aspect_name,temp_map);
                        aspect_map.put(aspect_name,new_aspect);
                    }
                }

            }
        }

        //manually include the application aspect using the embedding

        if(aspect_map.get("application1")==null)
        {
            LinkedHashMap<String,Double> temp_map = new LinkedHashMap<String,Double>();
            int cc=0;
            for(Map.Entry<String, Double> entry: cosine_distance_application_map_list)
            {
                //System.out.println(entry.getKey()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));

                if(entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey())!=null)
                {
                     cc++;
                    if(cc>20) break;
                    temp_map.put(entry.getKey(), entry.getValue());
                   // System.out.println(entry.getValue()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                }

            }
           // System.out.println("size of temp_map is: "+temp_map.size());
            Aspect new_aspect = new Aspect("application1", temp_map);
            aspect_map.put("application1", new_aspect);
        }
        else
        {
            Aspect temp_map = aspect_map.get("application1");
         //   for(String entity : temp_map.getMap_of_entities().keySet())
              //  System.out.println(entity+" ("+temp_map.getMap_of_entities().get(entity)+")"+"\n");
        }

        //manually include technique aspect using the embedding

        if(aspect_map.get("technique1")==null)
        {
            LinkedHashMap<String,Double> temp_map = new LinkedHashMap<String,Double>();
            int cc=0;
            for(Map.Entry<String, Double> entry: cosine_distance_technique_map_list)
            {

              //  System.out.println(entry.getKey()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                if(entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey())!=null)
                {
                     cc++;
                     if(cc>20) break;
                    temp_map.put(entry.getKey(), entry.getValue());
                    // System.out.println(entry.getValue()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                }

            }
         //   System.out.println("size of temp_map is: "+temp_map.size());
            Aspect new_aspect = new Aspect("technique1", temp_map);
            aspect_map.put("technique1", new_aspect);
        }
        else
        {
            Aspect temp_map = aspect_map.get("technique1");
         //   for(String entity : temp_map.getMap_of_entities().keySet())
             //   System.out.println(entity+" ("+temp_map.getMap_of_entities().get(entity)+")"+"\n");
        }

        //manually include implementation aspect using the embedding

        if(aspect_map.get("implementation1")==null)
        {
            LinkedHashMap<String,Double> temp_map = new LinkedHashMap<String,Double>();
            int cc=0;
            for(Map.Entry<String, Double> entry: cosine_distance_implementation_map_list)
            {
               // cc++;
               // if(cc>10) break;
             //   System.out.println(entry.getKey()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                if(entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey())!=null)
                {
                     cc++;
                     if(cc>20) break;
                    temp_map.put(entry.getKey(), entry.getValue());
                    // System.out.println(entry.getValue()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                }

            }
           // System.out.println("size of temp_map is: "+temp_map.size());
            Aspect new_aspect = new Aspect("implementation1", temp_map);
            aspect_map.put("implementation1", new_aspect);
        }
        else
        {
            Aspect temp_map = aspect_map.get("implementation1");
          //  for(String entity : temp_map.getMap_of_entities().keySet())
            //    System.out.println(entity+" ("+temp_map.getMap_of_entities().get(entity)+")"+"\n");
        }

        if(aspect_map.get("algorithm1")==null)
        {
            LinkedHashMap<String,Double> temp_map = new LinkedHashMap<String,Double>();
            int cc=0;
            for(Map.Entry<String, Double> entry: cosine_distance_algorithm_map_list)
            {
                // cc++;
                // if(cc>10) break;
                //   System.out.println(entry.getKey()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                if(entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey())!=null)
                {
                    cc++;
                    if(cc>20) break;
                    temp_map.put(entry.getKey(), entry.getValue());
                    // System.out.println(entry.getValue()+"\t"+entity_idf_euclidean_distance_product_map_sorted.get(entry.getKey()));
                }

            }
            // System.out.println("size of temp_map is: "+temp_map.size());
            Aspect new_aspect = new Aspect("algorithm1", temp_map);
            aspect_map.put("algorithm1", new_aspect);
        }
        else
        {
            Aspect temp_map = aspect_map.get("algorithm1");
            //  for(String entity : temp_map.getMap_of_entities().keySet())
            //    System.out.println(entity+" ("+temp_map.getMap_of_entities().get(entity)+")"+"\n");
        }


        while((current_line = aspects_buffered_reader.readLine())!=null)
        {
            String[] aspect_name_id_pair = current_line.split("\\s+");
            String key = aspect_name_id_pair[1].replace('_',' ').toString();
            Integer value = Integer.parseInt(aspect_name_id_pair[0]);
            aspect_name_id_map.put(key,value);
        }
      //  System.out.println(" aspect map is created");




        query_string_kb_id = aspect_name_id_map.get(query_string);//When Query String is not present in kb embeddings ,it will return null,so handle that case in future when required
     //   System.out.println(" Query string is : "+query_string);
     //   System.out.println(" Query string Id is : "+query_string_kb_id);


        for(String aspect_name : aspect_map.keySet())
        {
            Integer aspect_id = aspect_name_id_map.get(aspect_name);
            if(aspect_id!=null)
                aspect_id_name_map.put(aspect_id,aspect_name);
        }
     //   System.out.println(" Aspect id name map is created\n");

        while((current_line = aspects_embedding_buffered_reader.readLine())!=null)
        {

            String[] embedding = current_line.split("\\s+");
            Integer key = Integer.parseInt(embedding[0]);
            ArrayList<Double> value = new ArrayList<Double>();
            for(int i=1;i<embedding.length;i++)
            {
                value.add(Double.valueOf(embedding[i]));
            }
            //System.out.println("Key found is "+key);
            String aspect_name = aspect_id_name_map.get(key);

            if(aspect_name!=null)
                aspect_name_embedding_map.put(aspect_name,value);

            if(key.equals(query_string_kb_id))
            {
                query_string_kb_embedding = value;
          //      System.out.println(" Query string kb embedding is "+value.toString());
            }

        }
        aspect_name_embedding_map.put("application1",query_string_kb_embedding);
        aspect_name_embedding_map.put("technique1",query_string_kb_embedding);
        aspect_name_embedding_map.put("implementation1",query_string_kb_embedding);
        aspect_name_embedding_map.put("algorithm1",query_string_kb_embedding);
      //  System.out.println(" Aspect name and embedding map is created \n");

        for(Map.Entry<String,Aspect>aspect:aspect_map.entrySet())
        {
            aspect.getValue().caluculateAverage();
            aspect.getValue().caluculateNumberOfEntities();
            Double cosine_distance = euclideanDistance(aspect_name_embedding_map.get(aspect.getKey()),query_string_kb_embedding);
            aspect.getValue().setCosine_distance_with_query_term(cosine_distance);
            aspect.getValue().caluculateAspectPriority();
        }

        LinkedHashMap aspect_map_sorted = sortHashMapByValues(aspect_map,true);


      //  System.out.println("The number of aspects are : "+aspect_map.size());
      //  System.out.println("The apsects are : \n");

        for(Object aspect : aspect_map_sorted.entrySet())
        {
         //   System.out.println(((Map.Entry)aspect).getValue());
         //   System.out.println("\n\n\n");
        }

        if(entity_idf_euclidean_distance_product_map_sorted.size()==0)
        {
          //  System.out.println(" Query embeding is null");
            for (Map.Entry<String, Double> entry : entity_score_map.entrySet()) {

                aspect_map.put(entry.getKey(),new Aspect(entry.getKey(),null));
            }
        }

      //  System.out.println(" Aspects are created ");

        return aspect_map_sorted;
    }
}
