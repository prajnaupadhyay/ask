package org.lemurproject.galago.core.tools;
public class SearchResultItemWithClusterNo {
    Search.SearchResultItem item;
    Integer cluster_no;

    public SearchResultItemWithClusterNo(Search.SearchResultItem item) {
        this.item = item;
    }

    public SearchResultItemWithClusterNo(Search.SearchResultItem item, Integer cluster_no) {
        this.item = item;
        this.cluster_no = cluster_no;
    }

    public Search.SearchResultItem getItem() {
        return item;
    }

    public void setItem(Search.SearchResultItem item) {
        this.item = item;
    }

    public Integer getCluster_no() {
        return cluster_no;
    }

    public void setCluster_no(Integer cluster_no) {
        this.cluster_no = cluster_no;
    }
}
