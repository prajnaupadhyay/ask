// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.utility.tools.Arguments;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;
import java.util.*;

/**
 *
 * @author sjh
 */
public class QuadAspectFile extends AppFunction {

    public static final Logger logger = Logger.getLogger("QuadAspectFile");

    public static void main(String[] args) throws Exception
    {
        (new QuadAspectFile()).run(Arguments.parse(args), System.out);
    }

    @Override
    public String getName() {
        return "quad-aspect-file";
    }

    @Override
    public String getHelpString() {
        return "";
    }

    @Override
    public void run(Parameters parameters, PrintStream out) throws Exception {
        List<ScoredDocument> results;
        HashSet<Long> documents = new HashSet<Long>();


        if (!(parameters.containsKey("query") || parameters.containsKey("queries")))
        {
            out.println(this.getHelpString());
            return;
        }


        // ensure we can print to a file instead of the commandline
        if (parameters.isString("outputFile"))
        {
            boolean append = parameters.get("appendFile", false);
            out = new PrintStream(new BufferedOutputStream(
                    new FileOutputStream(parameters.getString("outputFile"), append)), true, "UTF-8");
        }

        // get queries
        List<Parameters> queries;
        String queryFormat = parameters.get("queryFormat", "json").toLowerCase();
        switch (queryFormat)
        {
            case "json":
                queries = JSONQueryFormat.collectQueries(parameters);
                break;
            case "tsv":
                queries = JSONQueryFormat.collectTSVQueries(parameters);
                break;
            default: throw new IllegalArgumentException("Unknown queryFormat: "+queryFormat+" try one of JSON, TSV");
        }

        // open index
        Retrieval retrieval = RetrievalFactory.create(parameters);
        System.out.println("here");

        // record results requested
        int requested = (int) parameters.get("requested", 1000);

        // for each query, run it, get the results, print in TREC format
        HashSet<Integer> h = new HashSet<Integer>();

        HashMap<String, HashMap<Long, Double>> subquery_scores = new HashMap<String, HashMap<Long, Double>>();

        for (Parameters query : queries)
        {
            String queryText = query.getString("text");
            System.out.println("text of the query is: "+queryText);
            String queryNumber = query.getString("number");


            query.setBackoff(parameters);
            query.set("requested", requested);

            // option to fold query cases -- note that some parameters may require upper case
            if (query.get("casefold", false))
            {
                queryText = queryText.toLowerCase();
            }

            if (parameters.get("verbose", false))
            {
                logger.info("RUNNING: " + queryNumber + " : " + queryText);
            }

            // parse and transform query into runnable form
            Node root = StructuredQuery.parse(queryText);

            // --operatorWrap=sdm will now #sdm(...text... here)
            if(parameters.isString("operatorWrap"))
            {
                if(root.getOperator().equals("root"))
                {
                    root.setOperator(parameters.getString("operatorWrap"));
                }
                else
                    {
                    Node oldRoot = root;
                    root = new Node(parameters.getString("operatorWrap"));
                    root.add(oldRoot);
                }
            }
            //String file_name=parameters.getString("entitylist");
            Node transformed = retrieval.transformQuery(root, query);
            String query_text = transformed.getText().replaceAll("\\s{2,}", " ").trim();



            if (parameters.get("verbose", false))
            {
                logger.info("Transformed Query:\n" + transformed.toPrettyString());
            }

            // run query
            results = retrieval.executeQuery(transformed, parameters).scoredDocuments;
            System.out.println("\n"+results.size()+"\n");
           /* org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
            org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters);
            for(ScoredDocument n:results)
            {
                System.out.println(n.document);
                System.out.println(n.score);
            }*/
            HashMap<Long, Double> scores = new HashMap<Long, Double>();
            for(ScoredDocument n:results)
            {
                scores.put(n.document,n.score);
                documents.add(n.document);
            }
            subquery_scores.put(query_text,scores);
        }
        for(Long dd:documents)
        {
            out.print(dd+"\t");
            for (String query : subquery_scores.keySet())
            {
                //System.out.println("query is: "+query);
                out.print(subquery_scores.get(query).get(dd)+"\t");
            }
            out.print("\n");
        }

        if (parameters.isString("outputFile")) {
            out.close();
        }
    }

}
