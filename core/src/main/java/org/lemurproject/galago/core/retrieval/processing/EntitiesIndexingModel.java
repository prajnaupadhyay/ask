package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.io.*;
import org.lemurproject.galago.core.tools.*;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

public class EntitiesIndexingModel extends org.lemurproject.galago.core.retrieval.processing.ProcessingModel
{
    org.lemurproject.galago.core.retrieval.LocalRetrieval retrieval;
    org.lemurproject.galago.core.index.Index index;
    String file_name;
    String documents_indexed_file;
    HashMap<String, Integer> entities;
    BufferedWriter bw;
    HashSet<Long> documents_indexed;
    Trie trie;
    //static ArrayList<String> tags = new ArrayList<String>({"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "<document>", "</document>", "<item>", "</item>"});

    public EntitiesIndexingModel(org.lemurproject.galago.core.retrieval.LocalRetrieval lr, String file_name, String index_name, String documents_indexed_file) throws Exception
    {
        retrieval = lr;
        this.index = retrieval.getIndex();
        this.file_name = file_name;
        this.documents_indexed_file=documents_indexed_file;
        File f = new File(documents_indexed_file);
        if(!f.exists())
        {
            f.createNewFile();
        }

        BufferedReader br1 = new BufferedReader(new FileReader(file_name));
        String line;
        HashMap<String, Integer> entities = new HashMap<String, Integer>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            entities.put(tokens.get(1).replace("_"," ").replace("-"," "),Integer.parseInt(tokens.get(0)));
        }
        System.out.println("code for hidden markov model is: "+entities.get("hidden markov model"));
        HashSet<Long> documents_indexed1 = new HashSet<Long>();
        BufferedReader br2 = new BufferedReader(new FileReader(documents_indexed_file));
        while((line=br2.readLine())!=null)
        {
            documents_indexed1.add(Long.parseLong(line));
        }
        this.documents_indexed = documents_indexed1;
        br2.close();
        this.entities = entities;
        System.out.print("initialized entitiesindexing model\n");
        BufferedWriter bw = new BufferedWriter(new FileWriter(index_name,true));
        this.bw = bw;
        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String keyword : entities.keySet())
        {
            trie_builder = trie_builder.addKeyword(keyword);
        }
        Trie trie1 = trie_builder.build();
        this.trie = trie1;
    }

    public org.lemurproject.galago.core.retrieval.ScoredDocument[] execute(org.lemurproject.galago.core.retrieval.query.Node queryTree, org.lemurproject.galago.utility.Parameters queryParams) throws Exception
    {
        System.out.println("this is the query tree: "+queryTree.isText());
        System.out.println("size of documents indexed: "+documents_indexed.size());
        // This model uses the simplest ScoringContext
        ScoringContext context = new ScoringContext();

        ScoreIterator iterator =
                (ScoreIterator) retrieval.createIterator(queryParams,
                        queryTree);

         int cc=0;
        ArrayList<ScoredDocument> scoredlist = new ArrayList<ScoredDocument>();
        Document.DocumentComponents p1 = new Document.DocumentComponents();
        org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(queryParams);

        while (!iterator.isDone())
        {
            cc++;
            long document = iterator.currentCandidate();
            //System.out.println(document);
            context.document = document;
           // System.out.println("something to print");
            iterator.syncTo(document);
            if (iterator.hasMatch(context))
            {
                if(!documents_indexed.contains(document))
                {
                    documents_indexed.add(document);
                    scoredlist.add(new ScoredDocument(context.document, 1.0));
                    String docname = retrieval.getDocumentName(context.document);
                    org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
                    //System.out.println(d.text);
                   // String dtext1 = d.text.replace("<", " <").replace(">", "> ").toLowerCase();
                    String dtext = (d.contentForXMLTag("title")+" "+d.contentForXMLTag("paperAbstract")).toLowerCase().replace("<strong>","").replace("</strong>","").replaceAll("[(),.!?;:]", " $0 ").replaceAll("[^A-Za-z0-9. ]+", " $0 ");
                    Collection<Emit> named_entity_occurences = trie.parseText(dtext);
                    Set<String> named_entities = new HashSet<String>(); // Here set is created additionally to handle cases where named entity is present more than once in a document
                   /* if(named_entity_occurences.size()==0)
                    {
                        System.out.println(document+" "+dtext);
                    }*/
                    for (Emit named_entity_occurence : named_entity_occurences)
                    {
                        bw.write("\n"+entities.get(named_entity_occurence.getKeyword()) + "\t" + document);
                    }

                }

            }
            iterator.movePast(document);

        }
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(documents_indexed_file));
        for(Long l:documents_indexed)
        {
            bw1.write(l+"\n");
        }
        bw1.close();
        bw.close();
        System.out.print("size of the results = "+cc);
           return scoredlist.toArray(new ScoredDocument[0]);
    }



}
