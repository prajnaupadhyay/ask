package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.tools.Arguments;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.*;

public class Parse extends AppFunction
{
    public static final Logger logger = Logger.getLogger("BatchSearch");

    public static void main(String[] args) throws Exception
    {
        (new org.lemurproject.galago.core.tools.apps.GetGoldStandard()).run(Arguments.parse(args), System.out);
    }

    @Override
    public String getName()
    {
        return "parse";
    }

    @Override
    public String getHelpString()
    {
        return this.getName()+" --input=<input file form which specific characters have to be replaced>";

    }

    public String contentForXMLTag(String tag, String line)
    {
        String start_tag = "<"+tag+">";
        String end_tag = "</"+tag+">";
        int start = line.indexOf(start_tag);
        int end = line.indexOf(end_tag);
        if(start>=0 && end >=0)
        {
            return line.substring(start+start_tag.length(),end);
        }
        else
        {
            return "";
        }
    }

    @Override
    public void run(Parameters parameters, PrintStream out) throws Exception {
        List<org.lemurproject.galago.core.retrieval.ScoredDocument> results;

        String inputfile="";
        if(parameters.containsKey("inputfile"))
        {
            inputfile = parameters.getString("inputfile");
        }
        else
        {
            out.println(this.getHelpString());
            return;
        }

        BufferedReader br = new BufferedReader(new FileReader(inputfile));
        BufferedWriter bw = new BufferedWriter(new FileWriter(inputfile.replace(".txt","")+"_clean.txt"));
        String line;
        while((line=br.readLine())!=null)
        {
            ArrayList<String> tokens = new ArrayList<String>();
            StringTokenizer tok = new StringTokenizer(line,"\t");
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
            }
            if(tokens.size()!=4) continue;
            //System.out.println(tokens.size());
            String title = contentForXMLTag("title",tokens.get(1));
            String abstract1 = contentForXMLTag("paperabstract",tokens.get(1));
            bw.write(title+" "+abstract1+"\t"+tokens.get(2)+"\t"+tokens.get(3)+"\n");
        }
        bw.close();


    }
}
