package org.lemurproject.galago.core.retrieval.processing;


import org.lemurproject.galago.core.retrieval.processing.ProcessingModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.CountIterator;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import java.io.*;
import java.util.*;

import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

public class AspectMixtureModel extends ProcessingModel
{
    org.lemurproject.galago.core.retrieval.LocalRetrieval retrieval;
    org.lemurproject.galago.core.index.Index index;
    public String aspect_name;
    public HashMap<Integer, Double> mixture;
    public HashMap<Integer, Double> term_statistics;
    public HashMap<Long, HashMap<Integer, Double>> freq_prob;
    public String scorer;
    public int feedback;
    HashSet<Long> documents;



    public AspectMixtureModel(org.lemurproject.galago.core.retrieval.LocalRetrieval lr, String word_index_file, String aspect_name, String mixturefile, String scorer, String termstatistics, int feedback, String document_file) throws Exception
    {
        HashMap<Integer, Double> mixture = new HashMap<Integer, Double>();
        HashMap<Integer, Double> mixture_prob = new HashMap<Integer, Double>();
        HashMap<Integer, Double> term_statistics = new HashMap<Integer, Double>();
        BufferedReader br1 = new BufferedReader(new FileReader(mixturefile));
        BufferedReader br3 = new BufferedReader(new FileReader(termstatistics));
        String line;
        double mix_sum=0.0;
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            mixture.put(Integer.parseInt(tokens.get(0)),Double.parseDouble(tokens.get(1)));
            mix_sum=mix_sum+Double.parseDouble(tokens.get(1));

        }
        for(int s:mixture.keySet())
        {
            mixture_prob.put(s,mixture.get(s)/mix_sum);
        }
        System.out.println("read mixture file");
        double total=0.0;

        while((line=br3.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            if(tokens.size()!=4) continue;

            term_statistics.put(Integer.parseInt(tokens.get(3)),Double.parseDouble(tokens.get(1)));
           // term_statistics_mapped.put(tokens.get(0),Integer.parseInt(tokens.get(3)));
            total = total + Double.parseDouble(tokens.get(1));
        }
        System.out.println("read termstatistics file");

        for(int s1:term_statistics.keySet())
        {
            term_statistics.put(s1,term_statistics.get(s1)/total);
        }



        System.out.println("read entity mappings");

        HashMap<Long, HashMap<Integer, Double>> word_frequencies = new HashMap<Long, HashMap<Integer, Double>>();

        br1 = new BufferedReader(new FileReader(word_index_file));
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=3) continue;
            long doc = Long.parseLong(tokens.get(1));
            int word = Integer.parseInt(tokens.get(0));
            if(word_frequencies.get(Long.parseLong(tokens.get(1)))==null)
            {
                word_frequencies.put(doc, new HashMap<Integer, Double>());
            }

            HashMap<Integer, Double> h = word_frequencies.get(doc);
            if(h.get(word)==null)
            {
                h.put(word,Double.parseDouble(tokens.get(2)));
            }

            word_frequencies.put(Long.parseLong(tokens.get(1)),h);
        }

        HashSet<Long> documents = new HashSet<Long>();

        BufferedReader br4 = new BufferedReader(new FileReader(document_file));
        while((line=br4.readLine())!=null)
        {
            documents.add(Long.parseLong(line));
        }

        System.out.println("read word index file");

        System.out.println("computed word frequencies");

        this.retrieval = lr;
        this.index = retrieval.getIndex();
        this.freq_prob = word_frequencies;
        this.aspect_name = aspect_name;
        this.mixture = mixture_prob;
        this.scorer = scorer;
        this.term_statistics=term_statistics;
        this.feedback=feedback;
        this.documents = documents;

    }


    public org.lemurproject.galago.core.retrieval.ScoredDocument[] execute(org.lemurproject.galago.core.retrieval.query.Node queryTree, org.lemurproject.galago.utility.Parameters queryParams) throws Exception
    {
        System.out.println("in execute");
        System.out.println("Completed reading auxillary data structures: "+System.nanoTime());
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        System.out.println("feedback: "+feedback);

        org.lemurproject.galago.core.retrieval.processing.ScoringContext context = new org.lemurproject.galago.core.retrieval.processing.ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        System.out.println(query_text);
        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());
        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue1 = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());


        HashMap<Integer, Double> mix_back_prob = new HashMap<Integer, Double>();
        double min=1.0;
        double total_mix=0.0;
        for(int s1:mixture.keySet())
        {
            total_mix = total_mix + mixture.get(s1);
            if(mixture.get(s1)<min) min= mixture.get(s1);
            mix_back_prob.put(s1, term_statistics.get(s1));
        }

        HashSet<Long> documents1 = new HashSet<Long>();



        System.out.println("mixture total = "+total_mix);
        org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator iterator = (org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator) retrieval.createIterator(queryParams, queryTree);
        org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator iterator1 = (org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator) retrieval.createIterator(queryParams, queryTree);


        boolean flag=false;

        System.out.println("documents size: "+documents.size());
        int count=feedback;
        int cc=0;
        double mu = iterator.mu;
        double background = iterator.background;
        int length = iterator.lengthsIterator.length(context);
        System.out.println("background is: "+background);
        int num_threads = (Runtime.getRuntime().availableProcessors()+1)/2;

        ThreadPoolExecutor executor = new ThreadPoolExecutor(num_threads, count, Long.MAX_VALUE, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(count));

        ConcurrentHashMap<Long, Double> scores = new ConcurrentHashMap<Long, Double>();
        //while(!iterator.isDone())
       for(Long document:documents)
        {
            if(freq_prob.get(document)==null)
            {
                System.out.println(document);
                continue;
            }
            //System.out.println(document);
            HashMap<Integer, Double> mixture1 = new HashMap<Integer, Double>(mixture);

            HashMap<Integer, Double> background_prob=new HashMap<Integer, Double>(mix_back_prob);
            HashMap<Integer, Double> hh2 = new HashMap<Integer, Double>();
            for(int ss:freq_prob.get(document).keySet())
            {
                if(term_statistics.get(ss)!=null)
                {
                    background_prob.put(ss, term_statistics.get(ss));
                    hh2.put(ss, freq_prob.get(document).get(ss));
                }

            }
            TaskParallelize t1 = new TaskParallelize(mixture1, hh2, scores, background_prob, document, scorer, min, mu);
            executor.execute(t1);

        }
    executor.shutdown();
    executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
     System.out.println(scores.size());
     for(Long d:scores.keySet())
     {
         double tot_score=scores.get(d);
         if ((queue.size() < feedback || queue.peek().score < tot_score))
         {
             org.lemurproject.galago.core.retrieval.ScoredDocument scoredDocument = new org.lemurproject.galago.core.retrieval.ScoredDocument(d, tot_score);
             if (annotate)
             {
                 scoredDocument.annotation = iterator.getAnnotatedNode(context);
             }
             queue.offer(scoredDocument);
         }
     }

        return toReversedArray(queue);
    }


}