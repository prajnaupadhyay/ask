package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.tupleflow.Utility;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.StreamCreator;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.core.aspects.Preprocess;



import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.*;

import java.io.*;


/**
 * Computes probability distribution over words given different aspects
 * @author jfoley
 */
public class ComputeProbabilityDistribution extends AppFunction {

    @Override
    public String getName() {
        return "compute-probability-distribution";
    }

    @Override
    public String getHelpString() {
        return "compute-probability-distribution aspect=<aspect name> evaluated=<location to file containing the evaluated results for the aspect> searched-results=<location to the file containing the search results for the aspect> stop-word=<location to file containing stop words> outfile=<output file where the distribution of words will be written>";
    }

    @Override
    public void run(Parameters parameters, PrintStream out) throws Exception {

       // Retrieval index = RetrievalFactory.create(p);
        Preprocess pr = new Preprocess();
        String aspect="";
        String evaluated="";
        String searched_results="";
        String stop_words="";
        String outfile="";

        if(parameters.containsKey("aspect"))
        {
            aspect = parameters.getString("aspect");
        }
        else
        {
            out.println("aspect name is missing");
            System.exit(0);
        }

        if(parameters.containsKey("evaluated"))
        {
            evaluated = parameters.getString("evaluated");
        }
        else
        {
            out.println("evaluated results file is missing");
            System.exit(0);
        }

        if(parameters.containsKey("searched-results"))
        {
            searched_results = parameters.getString("searched-results");
        }
        else
        {
            out.println("searched results file is missing");
            System.exit(0);
        }

        if(parameters.containsKey("stop-words"))
        {
            stop_words = parameters.getString("stop-words");
        }
        else
        {
            out.println("stop words file is missing");
            System.exit(0);
        }
        if(parameters.containsKey("outfile"))
        {
            outfile = parameters.getString("outfile");
        }
        else
        {
            out.println("outfile is missing");
            System.exit(0);
        }
       HashMap<String, Double> aspect_prob_dist = pr.estimateProbabilitiesUsingGroundTruth(evaluated, searched_results, stop_words, aspect);
        BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
        for(String s:aspect_prob_dist.keySet())
        {
            bw.write(s+"\t"+aspect_prob_dist.get(s)+"\n");
        }
        bw.close();
    }
}
