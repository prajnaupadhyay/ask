package org.lemurproject.galago.core.retrieval.processing;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;
import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.tools.Search;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.*;
import java.util.*;

public class GetDocumentList extends ProcessingModel
{
    LocalRetrieval retrieval;
    Index index;

    String documents_indexed_file;



    //static ArrayList<String> tags = new ArrayList<String>({"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "<document>", "</document>", "<item>", "</item>"});

    public GetDocumentList(LocalRetrieval lr, String documents_indexed_file) throws Exception
    {
        retrieval = lr;
        this.index = retrieval.getIndex();
        this.documents_indexed_file=documents_indexed_file;
        File f = new File(documents_indexed_file);
        if(!f.exists())
        {
            f.createNewFile();
        }
    }

    public ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception
    {
        System.out.println("this is the query tree: "+queryTree.isText());
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        // This model uses the simplest ScoringContext
        ScoringContext context = new ScoringContext();
        FixedSizeMinHeap<ScoredDocument> queue = new FixedSizeMinHeap<>(ScoredDocument.class, 1000, new ScoredDocument.ScoredDocumentComparator());

        ScoreIterator iterator =
                (ScoreIterator) retrieval.createIterator(queryParams, queryTree);

         int cc=0;
        ArrayList<ScoredDocument> scoredlist = new ArrayList<ScoredDocument>();
        Document.DocumentComponents p1 = new Document.DocumentComponents();
        Search s = new Search(queryParams);
        HashSet<Long> documents_indexed=new HashSet<Long>();
       // BufferedWriter bw1 = new BufferedWriter(new FileWriter(documents_indexed_file, true));

        while (!iterator.isDone())
        {
            cc++;
            long document = iterator.currentCandidate();
            //System.out.println(document);
            context.document = document;
           // System.out.println("something to print");
            iterator.syncTo(document);
            if (iterator.hasMatch(context))
            {
                double score = iterator.score(context);

                if (queue.size() < 1000 || queue.peek().score < score)
                {
                    ScoredDocument scoredDocument = new ScoredDocument(document, score);

                    queue.offer(scoredDocument);
                }
               // if(!documents_indexed.contains(document))
            }
            iterator.movePast(document);
        }

        System.out.print("size of the results = "+cc);
           return toReversedArray(queue);
    }



}
