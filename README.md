# ASK: Aspect-based academic Search using domain-specific KBs

This package consists of a set of classes for the task of aspect-based academic search. The major structure of the code has been borrowed from Galago. This is a maven project and should be compiled from the location 'galago_aspect1/galago-3.13' using the following command:

mvn -e install -DskipTests

This will create an executable 'galago' at the location galago_aspect1/galago-3.13/core/target/appassembler/bin/galago. Once this executable has been compiled, we are now ready to usethe following functions:

## Retrieve documents
The first step is to retrieve documents for a query as well as the aspect. This module retrieves top-1000 relevant documents for a query/aspect.


## Compute query independent probability distribution
This component estimates the query independent or aspect-dependent component. It takes the gold standard as the input consisting of: i) evaluated results, ii) results collected heuristically for the aspect using the previous step. Run the following command

compute-probability-distribution aspect=<aspect name> evaluated=<location to file containing the evaluated results for the aspect> searched-results=<location to the file containing the search results for the aspect> stop-word=<location to file containing stop words> outfile=<output file where the distribution of words will be written>

where the arguments are self-explanatory

## Compute mixture probability distribution
This component computes the mixture distribution for each query and aspect. Run the following command:

compute-mixture-distribution word_prob_file=<file containing the probability distribution estimated for the aspect> entity_prob_file=<file containing the pairwise probability of entities likely to be connected by the relationship described by the aspect estimated using PRA algorithm from TeKnowbase.> entity_prob_file1=<file containing the pairwise probability of entities likely to be connected by the relationship described by the aspect estimated using MetaPath2Vec algorithm from TeKnowbase.> termstatistics=<background probability distribution file> miiiixturefile=<file to store the mixture pobability distribution> lamda1=<parameter to mix query dependent and query independent component> lambda2=<parameter to mix the two inferencing components> --query=<query>







